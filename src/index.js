import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Game from "./Game";
import PlayerInfo from "./PlayerInfo";

class App extends React.Component {
  render() {
    return (
      <>
        <PlayerInfo />
        <Game />
      </>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
