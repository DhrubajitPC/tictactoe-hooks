import React, { Component } from "react";

export default class PlayerInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playerOne: "",
      playerTwo: ""
    };

    this.ref = React.createRef();
  }

  componentDidMount() {
    this.ref.current.focus();
  }

  render() {
    return (
      <>
        <div>
          <label>Player 1 name: </label>
          <input
            ref={this.ref}
            onChange={event => this.setState({ playerOne: event.target.value })}
          />
        </div>
        <div>
          <label>Player 2 name: </label>
          <input
            onChange={event => this.setState({ playerTwo: event.target.value })}
          />
        </div>
        <div>
          {this.state.playerOne && <p>{this.state.playerOne} is playing X</p>}
          {this.state.playerTwo && <p>{this.state.playerTwo} is playing O</p>}
        </div>
      </>
    );
  }
}
